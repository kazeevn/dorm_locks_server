from django.shortcuts import render
from django.conf import settings
from rest_framework import permissions
from dorm_locks_server.server.models import User, Card, RestrictedLocation, PublicLocation
from rest_framework import viewsets
from rest_framework.views import APIView
from dorm_locks_server.server.serializers import (
    UserSerializer,
    RestrictedLocationSerializer,
    CardSerializer,
    PublicLocationSerializer)
from dorm_locks_server.server.models import Card, Location
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

class RestrictedLocationsViewSet(viewsets.ModelViewSet):
    queryset = RestrictedLocation.objects.all()
    serializer_class = RestrictedLocationSerializer

class PublicLocationsViewSet(viewsets.ModelViewSet):
    queryset = PublicLocation.objects.all()
    serializer_class = PublicLocationSerializer

class CardViewSet(viewsets.ModelViewSet):
    queryset = Card.objects.all()
    serializer_class = CardSerializer

class LocksPermission(permissions.BasePermission):
    """Checks whether the requesting user belongs to LOCKS_GROUP group."""
    def has_permission(self, request, view):
        return request.user.groups.filter(name=settings.LOCKS_GROUP).exists()

class CheckAccess(APIView):
    permission_classes = (LocksPermission, )
    def get(self, request, location):
        if 'mifare_id' not in request.query_params:
            return Response(False, status=status.HTTP_400_BAD_REQUEST)
        location = Location.objects.filter(name=location)
        if not location.exists():
            return Response(False, status=status.HTTP_404_NOT_FOUND)
        card = Card.objects.filter(mifare_id=request.query_params['mifare_id'])
        if not card.exists():
            return Response(False, status=status.HTTP_403_FORBIDDEN)
        user = card.get().user
        if not user:
            return Response(False, status=status.HTTP_403_FORBIDDEN)
        if location.get_subclass().check_access(user):
            return Response(True)
        else:
            return Response(False, status=status.HTTP_403_FORBIDDEN)
