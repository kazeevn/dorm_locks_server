import re
from itertools import imap
from model_utils.managers import InheritanceManager
import django.contrib.auth.models
from django.contrib import admin
from django.db import models
from django.db.models import (
    CharField,
    IntegerField,
    ForeignKey,
    ManyToManyField,
    Model,
    DateTimeField,
    BooleanField,
    OneToOneField)

class Location(Model):
    name = CharField(max_length=255, unique=True, primary_key=True)
    objects = InheritanceManager()

    def __unicode__(self):
        return self.name

class PublicLocation(Location):
    group_regexp = CharField(max_length=255)
    dorm_regexp = CharField(max_length=255)

    def check_access(self, user):
        return re.match(self.group_regexp, user.group) or \
          re.match(self.dorm_regexp, str(user.dorm))

class RestrictedLocation(Location):

    def check_access(self, user):
        return user.permissions.filter(pk=self.pk)

    def grant_access(self, user):
        user.permissions.add(self)

class User(Model):
    first_name = CharField(max_length=255)
    middle_name = CharField(max_length=255)
    last_name = CharField(max_length=255)
    mipt_id = IntegerField(unique=True, primary_key=True)
    room = CharField(max_length=10)
    dorm = IntegerField()
    group = CharField(max_length=10)
    permissions = ManyToManyField(RestrictedLocation,
                                related_name="users",
                                blank=True)
    date_joined = DateTimeField(auto_now_add=True)

    def add_card(self, mifare_id):
        new_card = Card(mifare_id=mifare_id, user=self)
        new_card.save()

    def __unicode__(self):
        return "%s %s %s, %s group" % (
            self.first_name,
            self.middle_name,
            self.last_name,
            self.group)

class Card(Model):
    mifare_id = CharField(max_length=255, unique=True, primary_key=True)
    user = ForeignKey(User, related_name='cards', null=True, blank=True)

    def __unicode__(self):
        return self.mifare_id

for model in (PublicLocation, RestrictedLocation, Card, User):
    admin.site.register(model)
