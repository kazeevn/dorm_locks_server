from rest_framework import serializers
from dorm_locks_server.server.models import User, Card, RestrictedLocation, PublicLocation

class CardSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Card
        fields = ('mifare_id', 'user')

class RestrictedLocationSerializer(serializers.HyperlinkedModelSerializer):
    users = serializers.PrimaryKeyRelatedField(many=True, queryset=User.objects.all())
    class Meta:
        model = RestrictedLocation
        fields = ('name', 'users')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    permissions = serializers.PrimaryKeyRelatedField(many=True,
                                                     queryset=RestrictedLocation.objects.all())
    cards = serializers.PrimaryKeyRelatedField(many=True, queryset=Card.objects.all())
    class Meta:
        model = User
        fields = ('first_name',
                  'middle_name',
                  'last_name',
                  'mipt_id',
                  'room',
                  'dorm',
                  'group',
                  'cards',
                  'permissions')

class PublicLocationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PublicLocation
        fields = ('name', 'dorm_regexp', 'group_regexp')
