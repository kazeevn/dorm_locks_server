from rest_framework import routers
from dorm_locks_server.server import views

from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^locations/(?P<location>\w+)/check_access$', views.CheckAccess.as_view()),
]
